#!/usr/bin/env python

import sys
import socket
import tipc
import cs

#srvaddr = (tipc.TIPC_ADDR_NAMESEQ, 2000, 10, 100, tipc.TIPC_NODE_SCOPE)
srvaddr = (tipc.TIPC_ADDR_NAMESEQ, 2000, 10, 100)
#srvaddr = (tipc.TIPC_ADDR_NAME, 2000, 10, 0)
#srvaddr = (tipc.TIPC_ADDR_NAME, 2000, 10)  # broken

def srv():
	fd = tipc.socket(socket.SOCK_RDM)
	fd.bind(srvaddr)
	print 'server started, addr:', fd.getsockname()

	buf, addr = fd.recvfrom(1024)
	while buf != "EXIT":
		print 'srv:', addr, buf
		buf, addr = fd.recvfrom(1024)
	print 'srv exit'

def cli():
	print 'waiting'
	evt = tipc.wait_for(srvaddr, timeout = 500)
	if not evt or evt[0] == tipc.TIPC_SUBSCR_TIMEOUT:
		print 'timeout'
		return
	print 'server up', evt

	fd = tipc.socket(socket.SOCK_RDM)
	print 'cli ready, addr:', fd.getsockname()
	l = sys.stdin.readline()
	while l:
		fd.sendto(l.strip(), srvaddr)
		l = sys.stdin.readline()
	fd.sendto('EXIT', srvaddr)
	print 'cli exit'


cs.run(srv, cli)

