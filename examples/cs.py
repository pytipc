
import os
import sys
import time
import threading


def withthreads(srv, cli):
	t1 = threading.Thread(target = srv)
	t1.start()
	cli()


def withfork(srv, cli):
	ppid = os.getpid()
	cpid = os.fork()
	if cpid < 0:
		raise
	elif cpid == 0:
		# child
		try:
			srv()
		finally:
			os.kill(ppid, 15)
	else:
		# parent
		try:
			cli()
		finally:
			os.kill(cpid, 15)


def run(srv, cli):
	if int(os.environ.get('USE_THREADS', '0')):
		withthreads(srv, cli)
	else:
		withfork(srv, cli)

