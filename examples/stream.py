#!/usr/bin/env python

import sys
import socket
import tipc
import cs

srvaddr = (tipc.TIPC_ADDR_NAME, 2000, 10, 0)

def srv():
	fd = tipc.socket(socket.SOCK_STREAM)
	fd.bind(srvaddr)
	print 'server started, addr:', fd.getsockname()

	fd.listen(5)
	conn, addr = fd.accept()
	print 'connected', addr

	buf = conn.recv(1024)
	while buf != "EXIT":
		print 'srv:', buf
		buf = conn.recv(1024)
	print 'srv exit'

def cli():
	print 'waiting'
	evt = tipc.wait_for(srvaddr, timeout = 500)
	if not evt or evt[0] == tipc.TIPC_SUBSCR_TIMEOUT:
		print 'timeout'
		return
	print 'server up', evt

	fd = tipc.socket(socket.SOCK_STREAM)
	print 'cli ready, addr:', fd.getsockname()
	fd.connect(srvaddr)

	l = sys.stdin.readline()
	while l:
		fd.send(l.strip())
		l = sys.stdin.readline()
	fd.send('EXIT')
	print 'cli exit'


cs.run(srv, cli)

