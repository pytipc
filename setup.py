#!/usr/bin/env python

from distutils.core import setup, Extension

tipc_ll = Extension("tipc_ll", sources = ['tipc_ll.c'])

setup(
	name = 'tipc',
	version = '20080906',
	description = "TIPC bindings",
	author = "Alberto Bertogli",
	author_email = "albertito@blitiri.com.ar",
	url = "http://blitiri.com.ar/p/pytipc",
	py_modules = ['tipc'],
	ext_modules = [tipc_ll]
)

